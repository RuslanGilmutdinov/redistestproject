import redis.clients.jedis.Jedis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {


    public static void main(String[] args) throws IOException {
        String filePath = "src/main/resources/anekdots.txt";
        Jedis jedis = new Jedis("localhost", 6379);
        HashMap<Double, String> anekdots = getHashMapFromFile(filePath);
        LinkedList<Double> sortedRating = getSortedRatings(anekdots);
        setAllToRedis(jedis, sortedRating, anekdots);
        PrintAllAnekdots(sortedRating, jedis);
    }

    public static void PrintAllAnekdots(LinkedList<Double> sortedRating, Jedis jedis) {
        for (Double rating : sortedRating) {
            System.out.println(jedis.get(rating.toString()));
        }
    }

    public static Pattern getPattern() {
        String regexp = "\\s[0-9]\\.[0-9]$|\\s[0-1][0-9]\\.[0-9]$";
        Pattern pattern = Pattern.compile(regexp);
        return pattern;
    }

    public static HashMap<Double, String> getHashMapFromFile(String filePath) throws FileNotFoundException {
        HashMap<Double, String> anekdotes = new HashMap<>();
        Scanner scanner = new Scanner(new File(filePath));
        String str = "";
        while (scanner.hasNextLine()) {
            str = scanner.nextLine();
            Matcher matcher = getPattern().matcher(str);
            if (matcher.find()) {
                anekdotes.put(Double.parseDouble(str.substring(matcher.start() + 1, matcher.end())), str.substring(0, matcher.start() - 1));
            }
        }
        return anekdotes;
    }

    public static LinkedList<Double> getSortedRatings(HashMap<Double, String> anekdots) {
        LinkedList<Double> sortedRating = new LinkedList<>();
        anekdots.forEach((rating, anekdot) -> sortedRating.addFirst(rating));
        sortedRating.sort((aDouble, t1) -> aDouble.compareTo(t1));
        return sortedRating;
    }

    public static void setAllToRedis(Jedis jedis, LinkedList<Double> sortedRating, HashMap<Double, String> anekdots) {
        for (Double rating : sortedRating) {
            jedis.set(rating.toString(), anekdots.get(rating));
        }
    }

}




