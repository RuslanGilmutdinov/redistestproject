package com.example.MyLabaProject.model;


import java.io.Serializable;


public class Anekdot implements Serializable {

    private static final long serialVersionUID = 1L;
    private String text;
    private Double rating;


    public Anekdot(String text, Double rating) {
        this.text = text;
        this.rating = rating;

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }



    @Override
    public String toString() {
        return "Anekdot{" +

                "text='" + text + '\'' +
                ", rating=" + rating +
                '}';
    }
}
