package com.example.MyLabaProject.queue;

public interface MessagePublisher {
    void publish(final String message);
}
