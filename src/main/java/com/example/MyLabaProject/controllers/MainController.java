package com.example.MyLabaProject.controllers;

import com.example.MyLabaProject.model.Anekdot;
import com.example.MyLabaProject.repo.RedisRepository;
import com.example.MyLabaProject.repo.RedisRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Controller
public class MainController {

    public Long id = 0l;

    @Autowired
    private RedisRepositoryImpl redisRepository;

    @GetMapping("/anekdots")
    public String anekMain(Model model) {
        Iterable<Anekdot> anekdots = redisRepository.findAll();
        model.addAttribute("anekdots", anekdots);
        return "anekdots-main";
    }

    @GetMapping("/anekdots/add")
    public String anekAdd(Model model) {
        return "anek-add";
    }

    @PostMapping("/anekdots/add")
    public String anekPostAdd(@RequestParam String text, Model model) {
        Anekdot anekdot = new Anekdot(text, 0.0);
        redisRepository.add(anekdot);
        return "redirect:/anekdots";
    }


    @GetMapping("/anekdots/edit/{text}")
    public String anekEdit(@PathVariable(value = "text") String text, Model model) {
        //TO DO check existing
        Pattern pattern = Pattern.compile("^[0-9][0-9]\\.[0-9]|^[0-9]\\.[0-9]");
        Matcher matcher = pattern.matcher(text);
        matcher.find();

        Anekdot anekdot = redisRepository.findAnekdot(text.substring(matcher.end()), Double.parseDouble(text.substring(matcher.start(), matcher.end())));
        ArrayList<Anekdot> anekdots = new ArrayList<>();
        anekdots.add(anekdot);
        model.addAttribute("anekdots", anekdots);
        return "anek-edit";
    }

    @PostMapping("/anekdots/edit/{text}")
    public String anekUpdate(@PathVariable(value = "text") String text, @RequestParam Double newRating, Model model) {
        Pattern pattern = Pattern.compile("^[0-9][0-9]\\.[0-9]|^[0-9]\\.[0-9]");
        Matcher matcher = pattern.matcher(text);
        matcher.find();

        Double oldRating = Double.parseDouble(text.substring(matcher.start(), matcher.end()));

        Anekdot anekdot = redisRepository.findAnekdot(text.substring(matcher.end()), Double.parseDouble(text.substring(matcher.start(), matcher.end())));

        redisRepository.update(anekdot, newRating);

        return "redirect:/anekdots";
    }


}
