package com.example.MyLabaProject.repo;

import com.example.MyLabaProject.model.Anekdot;

import java.util.Map;

public interface RedisRepository {

    Iterable<Anekdot> findAll();

    /**
     * Add key-value pair to Redis.
     */
    void add(Anekdot anekdot);

    /**
     * Delete a key-value pair in Redis.
     */
    void delete(String text, Double rating);

    /**
     * find a movie
     */
    Anekdot findAnekdot(String text, Double rating);


    //update anekdot
    void update(Anekdot anekdot, Double newRating);
}
