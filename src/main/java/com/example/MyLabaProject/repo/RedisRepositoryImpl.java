package com.example.MyLabaProject.repo;

import com.example.MyLabaProject.model.Anekdot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Repository;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Repository
public class RedisRepositoryImpl implements RedisRepository {


    private static final String K = "anekdots";
//    public ClassLoader classLoader = getClass().getClassLoader();

    private static final String filePath = "anekdots.txt";
    private static final String fileRegexp = "\\s[0-9]\\.[0-9]$|\\s[0-1][0-9]\\.[0-9]$";

    private RedisTemplate<String, Object> redisTemplate;
    private HashOperations hashOperations;
    private ZSetOperations<String, Object> zSetOperations;

    @Autowired
    public RedisRepositoryImpl(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init() throws FileNotFoundException {
        zSetOperations = redisTemplate.opsForZSet();

        HashMap<Double, String> anekdotes = new HashMap<>();
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(filePath);
        Scanner scanner = new Scanner(inputStream, "UTF-8");
//        File file = new File(String.valueOf(classLoader.getResourceAsStream(filePath)));
//        Scanner scanner = new Scanner(file);

//        Scanner scanner = new Scanner(new File(filePath));

        String str = "";
        while (scanner.hasNextLine()) {
            str = scanner.nextLine();
            Matcher matcher = Pattern.compile(fileRegexp).matcher(str);
            if (matcher.find()) {
                anekdotes.put(Double.parseDouble(str.substring(matcher.start() + 1, matcher.end())), str.substring(0, matcher.start()));
            }
        }
        anekdotes.forEach((rating, text) -> zSetOperations.add(K, rating.toString() + text, rating));
    }

    @Override
    public Iterable<Anekdot> findAll() {
        Pattern pattern = Pattern.compile("^[0-9][0-9]\\.[0-9]^|[0-9]\\.[0-9]");

        Set<Object> set = zSetOperations.range(K, 0, -1);
        ArrayList<Anekdot> arrayList2 = new ArrayList<>();

        for (Object o : set) {
            Matcher matcher = pattern.matcher(o.toString());
            matcher.find();

            Anekdot tempAnekdot = new Anekdot(o.toString().substring(matcher.end()), Double.parseDouble(o.toString().substring(0, matcher.end())));
            arrayList2.add(tempAnekdot);
        }


        return arrayList2;
    }

    @Override
    public void add(Anekdot anekdot) {
        zSetOperations.add(K, anekdot.getRating() + anekdot.getText(), anekdot.getRating());

    }

    @Override
    public void delete(String text, Double rating) {

        zSetOperations.remove(K, rating.toString() + text);

    }

    @Override
    public Anekdot findAnekdot(String text, Double rating) {

        Anekdot a = new Anekdot(text, rating);
        return a;
    }


    @Override
    public void update(Anekdot anekdot, Double newRating) {

        delete(anekdot.getText(), anekdot.getRating());

        anekdot.setRating(newRating);
        add(anekdot);
    }
}
