Собираем проект: mvn clean install

В папке с проектом создаем образ приложения: docker build -f Dockerfile -t doclab .

Запускаем redis: docker run -d --name redis -p 6379:6379 redis

Запускаем контейнер из образа приложения:  docker run --name doclab -p 8080:8080 --link redis:redis doclab

на http://localhost:8080/anekdots запустилось приложение

Первоначально анекдоты с рейтингами загружаются из anekdots.txt, их рейтинг можно править, также можно добавлять свои анекдоты(их первоначальный рейтинг будет 0.0)


Используются ZSetOperations, что дает сортированное множество