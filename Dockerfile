FROM openjdk:8
ADD target/MyLabaProject-0.0.1-SNAPSHOT.jar MyLabaProject-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "MyLabaProject-0.0.1-SNAPSHOT.jar"]